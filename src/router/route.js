import { ethAccounts, ethCoinBase, ethBlockNumber, ethGetBlockByNumber } from '../controller/eth.controller.js';
import index from '../controller/index.controller.js';

export default function route( server ){
    server.get( "/", index );
    server.get( "/eth/coinbase", ethCoinBase );
    server.get( "/eth/accounts", ethAccounts );
    server.get( "/eth/blocknumber", ethBlockNumber );
    server.get( "/eth/block/:number/:flag", ethGetBlockByNumber );
}
