import { CONFIG, PARAM } from '../config/config.js';

import axios from 'axios';

export function ethCoinBase(req, res){
    axios.post( CONFIG.rpcurl , PARAM.eth_coinbase.generate([]) ).then( response => {
        console.log("response",response.data);
        res.send(response.data.result);
    }).catch( error => {
        console.error(error);
    });
}

export function ethAccounts(req, res){
    axios.post( CONFIG.rpcurl , PARAM.eth_coinbase.generate([]) ).then( response => {
        console.log("response",response.data);
        res.send(response.data.result);
    }).catch( error => {
        console.error(error);
    });
}

export function ethBlockNumber(req, res){
    axios.post( CONFIG.rpcurl , PARAM.eth_blockNumber.generate([]) ).then( response => {
        console.log("response",response.data);
        var result = parseInt(response.data.result,16).toString();        
        res.send(result);
    }).catch( error => {
        console.error(error);
    });
}

export function ethGetBlockByNumber(req, res){
    var num = "0x" + parseInt(req.params.number).toString(16);
    var show_full_transaction = req.params.flag === "1" ? true : false;
    
    axios.post( CONFIG.rpcurl , PARAM.eth_getBlockByNumber.generate([num,show_full_transaction]) ).then( response => {
        console.log("response",response.data);
        res.send(response.data.result);
    }).catch( error => {
        console.error(error);
    });
}