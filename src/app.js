const express = require('express');
const app = express();
import route from './router/route.js';
import { CONFIG } from './config/config.js';

route(app);

app.listen( CONFIG.serverport, () => console.log('App running on port 8010'));