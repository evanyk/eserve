export const CONFIG = {
    rpcurl: "http://127.0.0.1:20303",
    serverport: 8010
};

export const PARAM = {
    eth_coinbase: {
        generate: ( params ) => {
            return {
                id: 64,
                jsonrpc: "2.0",
                method: "eth_coinbase",
                params: params
            }
        }
    },
    eth_accounts: {
        generate: ( params ) => {
            return {
                id: 1,
                jsonrpc: "2.0",
                method: "eth_accounts",
                params: params
            }
        }
    },
    eth_blockNumber: {
        generate: ( params ) => {
            return {
                id: 83,
                jsonrpc: "2.0",
                method: "eth_blockNumber",
                params: params
            }
        }
    },
    eth_getBlockByNumber: {
        generate: ( params ) => {
            return {
                id: 1,
                jsonrpc: "2.0",
                method: "eth_getBlockByNumber",
                params: params
            }
        }
    }
}