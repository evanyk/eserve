#!/bin/bash

geth --mine --minerthreads=2 --datadir ./data/60/01 --networkid 18001 --port 30303 --rpc --rpcport 20303 --rpcapi "web3,eth,net,personal" --rpccorsdomain "*" --rpcaddr "0.0.0.0" --ipcdisable console 2>./log/eEth.log

