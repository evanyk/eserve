# Summary
Connect local ethereum blockchain and support for front page to request

# Environment
OS: Ubuntu 18.04 LTS

# Tools
* geth
* node
  *  express
  *  axios
  *  babel (es6)

# Start geth
1> Initialize block chain <br>
   $geth --datadir ./data/60/01    //database directory <br>
         init ./eGenesis.json      //definition of the genesis block<br>
2> Start block chain service<br>
   $geth --mine --minerthreads=2   //start mining <br>
         --datadir ./data/60/01    //one peer <br>
         --networkid 10303 <br>
         --rpc --rpcport 20303 --rpcapi "web3,eth,net,personal"  //allow rpc <br>
         --rpccorsdomain "*" --rpcaddr "0.0.0.0" <br>
         --port 30303 <br>
         --ipcdisable  //allow multiple peers in one pc <br>
         console 2>./log/eEth.log   //output log <br>
         
# Start express server
$npm start<br>

# Reference
1> express document <br>
http://expressjs.com/en/guide/routing.html <br>
2> geth command document <br>
https://github.com/ethereum/go-ethereum/wiki/Command-Line-Options <br>
3> ethereum json rpc document <br>
https://github.com/ethereum/wiki/wiki/JSON-RPC <br>
